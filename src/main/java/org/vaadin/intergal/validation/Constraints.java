package org.vaadin.intergal.validation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;
import org.vaadin.intergal.validation.ValidationMetadata;

import com.fasterxml.jackson.databind.JsonNode;

import coop.intergal.AppConst;
import coop.intergal.PropertyController;
import coop.intergal.espresso.presutec.utils.JSonClient;
import coop.intergal.ui.util.UtilSessionData;
import coop.intergal.vaadin.rest.utils.DataService;
import coop.intergal.vaadin.rest.utils.DynamicDBean;
import coop.intergal.vaadin.rest.utils.RestData;


/**
 * Restricciones utilizadas con ValidationServiceImpl
 *
 * @author Javier
 *
 */
public class Constraints {

		
	static Hashtable<String, String> fieldNAmeAndFieldinUI = new Hashtable<String, String>();
	static Hashtable<String, String> validationAndCols = new Hashtable<String, String>();
	private static String keepCol0 = "";
	private static String keepCol1 = "";
	private static String keepValidationName;
	private static final String RESOURCE_FIELD_TEMPLATE = "CR-FormTemplate.List-FieldTemplate";

	private static String formatMessage(ValidationMetadata<?> metadata, String message) {
		String fieldName = metadata.getProperty("name");
		if (fieldName == null) {
			return message;
		} else {
			return fieldName + " " + message;
		}
	}

	public static String positive(Integer value, ValidationMetadata<?> metadata) {
		return greaterThan(value, metadata, "0");
	}

	public static String greaterThan(Integer value, ValidationMetadata<?> metadata, String arg) {
		boolean req = false;
		int separatorPos = arg.indexOf(";");
		if (separatorPos > -1)
		{
			String reqStr = arg.substring(separatorPos+1);	
			arg = arg.substring(0,separatorPos );
			if (reqStr.equals("true"))
				req = true;
		}
		if ( req)
		{
			String result = isRequired(value, metadata);
			if (result != null)
				return result;
		}
		int limit = Integer.parseInt(arg);
		if (value == null || value > limit) {
			return null;
		} else {
			return formatMessage(metadata, "debe ser mayor a " + limit);
		}
	}

	public static String lessThan(Integer value, ValidationMetadata<?> metadata, String arg) {
//		boolean req = false;
//		int separatorPos = arg.indexOf(";");
//		if (separatorPos > -1)
//		{
//			String reqStr = arg.substring(separatorPos+1);	
//			arg = arg.substring(0,separatorPos );
//			if (reqStr.equals("true"))
//				req = true;
//		}
//		if ( req)
//		{
//			String result = isRequired(value, metadata);
//			if (result != null)
//				return result;
//		}	
		Long limit = new Long (arg);
		if (value == null || value < limit) {
			return null;
		} else {
			return formatMessage(metadata, "debe ser menor a " + limit);
		}
	}
	public static String lessThan(String valueStr, ValidationMetadata<?> metadata, String arg) {
//		boolean req = false;
//		int separatorPos = arg.indexOf(";");
//		if (separatorPos > -1)
//		{
//			String reqStr = arg.substring(separatorPos+1);	
//			arg = arg.substring(0,separatorPos );
//			if (reqStr.equals("true"))
//				req = true;
//		}
		valueStr = valueStr.replace(",",".");
		Double  value = new Double (valueStr);
//		if ( req)
//		{
//			String result = isRequired(valueStr, metadata);
//			if (result != null)
//				return result;
//		}	
		Double limit = new Double (arg);
		
		if (value == null || value > limit) {
			return null;
		} else {
			return formatMessage(metadata, "debe ser menor a " + limit);
		}
	}


	public static String minLength(String value, ValidationMetadata<?> metadata, String arg) {
		int length = Integer.parseInt(arg);
		if (value == null || value.length() >= length) {
			return null;
		} else {
			return formatMessage(metadata, "debe contener al menos " + length + " caracteres");
		}
	}
	public static String isRequired(String value, ValidationMetadata<?> metadata) {
	//	int length = Integer.parseInt(arg);
	//	System.out.println("Got (String) " + value);
		if (value == null || value.length() >= 1) {
			return null;
		} else {
			return formatMessage(metadata, "Requerido");
		}
	}
	public static String isRequired(Double value, ValidationMetadata<?> metadata) {
	//	int length = Integer.parseInt(arg);
		if (value == null || value >= 0) {
			return null;
		} else {
			return formatMessage(metadata, "Requerido");
		}
	}
	public static String isRequired(BigDecimal value, ValidationMetadata<?> metadata) {
	//	int length = Integer.parseInt(arg);
		if (value == null) 
			return formatMessage(metadata, "Requerido");
		if (value.doubleValue() > 0) 
			return null;
		else {
			return formatMessage(metadata, "Requerido");
		}
	}
	public static String isRequired(Integer value, ValidationMetadata<?> metadata) {
	//	int length = Integer.parseInt(arg);
	//	if (value == null || value >= 0) {
	//	 System.out.println("Got (a) " + value);
		if (value == null) {
			return formatMessage(metadata, "Requerido");
		} else if (value >= 0) {
			return null;
		} else {
			return formatMessage(metadata, "Requerido");
		}
	}


	public static String notInFuture(LocalDate value, ValidationMetadata<?> metadata) {
		if (value == null || value.compareTo(LocalDate.now()) <= 0) {
			return null;
		} else {
			return formatMessage(metadata, "no debe ser posterior a la fecha actual");
		}
	}

	public static String compareDates(DynamicDBean bean, ValidationMetadata<?> metadata, String startDateCol, String endDateCol) {
		//Por simplicidad se ignora el argumento
		//El validador debería extraer los valores de interés a partir del parámetro delconstraint
		if (bean.getColDate(startDateCol) ==null || bean.getColDate(endDateCol)==null || bean.getColDate(startDateCol).compareTo(bean.getColDate(endDateCol)) < 0) {
			return null;
		} else {
			return "La fecha de inicio debe ser anterior a la fecha de finalizacion";
		}
	}
	public static String validateOneCol(DynamicDBean bean, ValidationMetadata<?> metadata, String colName) {
		//Por simplicidad se ignora el argumento
		//El validador debería extraer los valores de interés a partir del parámetro delconstraint
		if (bean.getCol(colName).equals("1111") ) {
			return null;
		} else {
			return "La fecha de inicio debe ser anterior a la fecha de finalizacion";
		}
	}
	public static String validateFromBackEnd(DynamicDBean bean, ValidationMetadata<?> metadata, String validationNameANDcache) {
		//Por simplicidad se ignora el argumento
		//El validador debería extraer los valores de interés a partir del parámetro delconstraint
		Boolean cache = true;
		if (validationNameANDcache.substring(validationNameANDcache.indexOf(",")).equals(",false"))
				cache = false;			
		String validationName = validationNameANDcache.substring(0, validationNameANDcache.indexOf(","));
		String Result = validateFromBackEnd(bean,validationName, cache);
		if (Result.equals("OK") ) {
			return null;
		} else {
			return Result;
		}
	}
	public static String warningFromUiValidation(DynamicDBean bean, ValidationMetadata<?> metadata, String validationNameANDcache) {
		//Por simplicidad se ignora el argumento
		//El validador debería extraer los valores de interés a partir del parámetro delconstraint
		Boolean cache = true;
		if (validationNameANDcache.substring(validationNameANDcache.indexOf(",")).equals(",false"))
				cache = false;			
		String validationName = validationNameANDcache.substring(0, validationNameANDcache.indexOf(","));
		String Result = warningFromUiValidation(bean,validationName);
		if (Result.equals("OK") ) {
			return null;
		} else {
			return Result;
		}
	}
	public static String warningFromBackEnd(DynamicDBean bean, ValidationMetadata<?> metadata, String validationNameANDcache) {
		//Por simplicidad se ignora el argumento
		//El validador debería extraer los valores de interés a partir del parámetro delconstraint
		Boolean cache = true;
		if (validationNameANDcache.substring(validationNameANDcache.indexOf(",")).equals(",false"))
				cache = false;			
		String validationName = validationNameANDcache.substring(0, validationNameANDcache.indexOf(","));
		String Result = warningFromBackEnd(bean,validationName, cache);
		if (Result.equals("OK") ) {
			return null;
		} else {
			return Result;
		}
	}

	private static String validateFromBackEnd(DynamicDBean bean, String validationName, Boolean cache) {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		String resourceName  = bean.getResourceName();
		String[] jSStringAndErrorTest = getJSFromBackEnd(resourceName,validationName, cache);
		if (jSStringAndErrorTest == null)
			return "ERROR en Validación, \"Nombre Validación BR\" ("+validationName+") no existe para la tabla ("+getTableNameFromResourceName(resourceName)+") correspondiente";
		String jSStringToProcess = createUsableJS(jSStringAndErrorTest[0], validationName, resourceName);
		String callJS = "valida("+getValuesFromBean(bean,resourceName, validationName)+")";
		String resultStr = "No validado";
		try {
			engine.eval(jSStringToProcess) ;
			Boolean result = (Boolean) engine.eval(callJS);
			if (result)
				resultStr="OK";
			else
				resultStr=jSStringAndErrorTest[1];
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR en Validación, \"Nombre Validación BR\" ("+validationName+") ->"+e.getMessage();
		} // prints
		return resultStr;
	}
	private static String warningFromBackEnd(DynamicDBean bean, String validationName, Boolean cache) {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		String resourceName  = bean.getResourceName();
		String[] jSStringAndErrorTest = getJSFromBackEnd(resourceName,validationName, cache);
		if (jSStringAndErrorTest == null)
			return "ERROR en Validación, \"Nombre Validación BR\" ("+validationName+") no existe para la tabla ("+getTableNameFromResourceName(resourceName)+") correspondiente";
		String jSStringToProcess = createUsableJS(jSStringAndErrorTest[0], validationName, resourceName);
		String callJS = "valida("+getValuesFromBean(bean,resourceName, validationName)+")";
		String resultStr = "No validado";
		try {
			engine.eval(jSStringToProcess) ;
			Boolean result = (Boolean) engine.eval(callJS);
			if (result)
				resultStr="OK";
			else
				resultStr="WARNING"+jSStringAndErrorTest[1];
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR en Validación, \"Nombre Validación BR\" ("+validationName+") ->"+e.getMessage();
		} // prints
		return resultStr;
	}
	private static String warningFromUiValidation(DynamicDBean bean, String validationName) {
		String filter ="";
		if (ifAlreadyWarning(bean, validationName) && (bean.getParams() == null || bean.getParams().indexOf("#VALWAYS#") == -1))  // #VALWAYS# is pass to validate always, normally when you press a button for validate
			return "OK";  
		int idxArroba = validationName.indexOf("@");
		String resourceUiV = validationName.substring(0,idxArroba);
//		int idxFilterInBean = validationName.indexOf("@PARENTFILTER");
//		if (idxFilterInBean > -1)
//		{
//			filter = bean.getFilter();
//		}
		int idxFilter = validationName.indexOf("@FILTER");
		if (idxFilter > -1)
		{
			filter = validationName.substring(idxFilter+7);
			int idxStartField = filter.indexOf("<<");
			int idxEndField = filter.indexOf(">>");
	
			String fieldTofilter = filter.substring(idxStartField+2,idxEndField);
			String colNameinUI = getColNameinUI(bean.getResourceName(),fieldTofilter);
			String valueToFilter = bean.getCol(colNameinUI) ;
			if (valueToFilter != null && valueToFilter.equals("null") == false)
				filter = filter.replace("<<"+fieldTofilter+">>",valueToFilter);
			else
				return "OK";  // if not value not validation 
		}
		int idxVal = validationName.indexOf("@VAL");  //@VAL<<nombreCampo>>
		if (idxVal > -1)
			{
			
			String colName = validationName.substring(idxVal+4);
			int idxStartCol = colName.indexOf("<<");
 			int idxEndCol = colName.indexOf(">>");
				
			String valueToCheck = "" ;
			String colNameinUIToGetValue ="";
			if (idxStartCol == -1)
				valueToCheck = validationName.substring(idxVal+4) ;
			else
			{
				colNameinUIToGetValue = colName.substring(idxStartCol+2,idxEndCol);
				String colNameinUIxVal = getColNameinUI(bean.getResourceName(),colNameinUIToGetValue);

				valueToCheck = bean.getCol(colNameinUIxVal) ;
			}	
			if (filter.indexOf("@VAL") > -1 )
			{
				if  (idxStartCol > -1)
					filter = filter.replace("@VAL<<"+colNameinUIToGetValue+">>","&valToCheck="+valueToCheck);
				else
					filter = filter.replace("@VAL"+valueToCheck,"&valToCheck="+valueToCheck);
			}
			else
				filter = filter+"&valToCheck="+valueToCheck;
			}
//		filter=filter+"&valToCheck="+valueToCheck;
		String resultStr = "OK";
		try {
			JsonNode resultRow = JSonClient.get(resourceUiV, filter,false,UtilSessionData.getCompanyYear()+PropertyController.pre_conf_param,1+"");
			if (resultRow.get("statusCode") == null)
				{
				if (resultRow.get("result") == null || resultRow.get("msgError")== null)
					DataService.get().showError(" Campos \"result\" o \"msgError\", no definidos en el recurso " +resourceUiV );
				else
				{	
					boolean result = resultRow.get("result").asBoolean();
					String msgError = resultRow.get("msgError").asText();
					if (result)
						resultStr="OK";
					else
						resultStr="WARNING"+msgError;
					}
				}
		} // prints
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultStr;
	}
	private static boolean ifAlreadyWarning(DynamicDBean bean, String validationName) {
		
		String col0 = bean.getCol0();
		String col1 = bean.getCol1();
//@@TODO		cambiar por un PK en el bean
		if (bean.getRowJSon() == null) // Is a New row then is validate each time 
			return false;
		keepCol0 = validationAndCols.get(validationName+"col0");
		keepCol1 = validationAndCols.get(validationName+"col1");
		if (keepCol0 == null)
			keepCol0 = "";
		if (keepCol1 == null)
			keepCol1 = "";
		if (col0 == null)
			col0 = "";
		if (col1 == null)
			col1 = "";
		if (col0 == keepCol0 && col1 == keepCol1)// && keepValidationName == validationName )
			return true;
		else
		{
			validationAndCols.put(validationName+"col0", col0);
			validationAndCols.put(validationName+"col1", col1);
			keepValidationName = validationName;
		}	
		return false;
	}

	private static String getValuesFromBean(DynamicDBean bean, String resourceName, String validationName) {
		String [] tokens = validationName.split(Pattern.quote("_"));
		int i = 1;
		String values = "'";
		while (tokens.length > i)
		{ 
			String field = tokens[i];
			String fieldNameInUI = fieldNAmeAndFieldinUI.get(resourceName+"#"+field);
			String value = bean.getCol(fieldNameInUI);
			values = values + value + "','";
			i ++;
		}
		if (values.indexOf("'true'")> -1)
			values = values.replace("'true'", "true");
		else if (values.indexOf("'false'")> -1)
			values = values.replace("'false'", "false");
		return values.substring(0, values.length()-2); // excludes last ",'"
	}

	private static String[] getJSFromBackEnd(String resourceName, String validationName, boolean cache) {
		try {
			String filter="\"name\"='"+validationName+"'";// AND \"entity_name\"='main:"+getTableNameFromResourceName(resourceName)+"'";
//			String q = "random word £500 bank $";
			String filterEncode = URLEncoder.encode(filter, StandardCharsets.UTF_8.toString());
			JsonNode rowRules = JSonClient.get("LACAdmin:rules",filterEncode,cache,PropertyController.pre_conf_param_metadata,1+"");
			for (JsonNode eachRow : rowRules)  {
				String[] s = new String[]{eachRow.get("rule_text1").asText(), eachRow.get("rule_text2").asText()};
				return s;
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		String[] s = new String[]{"if (row.ALQUILADO==\"S\" || row.ALQUILADO==\"N\" || !row.ALQUILADO || row.ALQUILADO==\" \")\r\n" + 
//				"    return true;\r\n" + 
//				"else\r\n" + 
//				"    return false;","ALQ. sólo admite los valores S, N o blanco" };
		return null;
	}

	private static String getTableNameFromResourceName(String resourceName) {
		if (resourceName.startsWith("CR-")) 
		{
			int idxEnd = resourceName.indexOf("__");
			String tableName = resourceName.substring(3);
			if (idxEnd > -1)
				tableName = resourceName.substring(3, idxEnd);
			return tableName;
		}
		if (resourceName.indexOf("List-") > -1 && resourceName.indexOf("List-") < 3) // could be xxList where xx is the order position for children tabs
		{
			int idxEnd = resourceName.indexOf("__");
			String tableName = resourceName.substring(5);
			if (idxEnd > -1)
				tableName = resourceName.substring(5, idxEnd);
			return tableName;
		}
		return resourceName;
	}

	private static String createUsableJS(String javascriptString, String fieldNames, String resourceName) {
		String [] tokens = fieldNames.split(Pattern.quote("_"));
		int i = 1;
		String functionDef = "function valida(";
		while (tokens.length > i)
		{ 
			String field = tokens[i];
			fillHTable(resourceName,field);
			javascriptString = javascriptString.replaceAll("row."+ field, "f"+i);
			functionDef = functionDef + "f"+i+",";
			i ++;
		}
		functionDef = functionDef.substring(0, functionDef.length()-1) + "){";
		return functionDef + javascriptString + "}";
	}
	private static void fillHTable(String resourceName, String field) {
		if (fieldNAmeAndFieldinUI.get(resourceName+"#"+field) == null)
		{
			fieldNAmeAndFieldinUI.put(resourceName+"#"+field, getColNameinUI(resourceName,field));
		}
		
	}

	private static String getColNameinUIOLD(String resourceName, String field) {
		String filter =  "tableName='"+resourceName+"'%20AND%20fieldName='"+field+"'";
//		ArrayList<String[]> rowsColList = new ArrayList<String[]>();
//		String[] fieldArr  = new String[1];
		
		DynamicDBean dynamicDBean = RestData.getOneRow(RESOURCE_FIELD_TEMPLATE,filter, PropertyController.pre_conf_param_metadata, null);
		if (dynamicDBean.getCol19() != null)
			return dynamicDBean.getCol19();
		else
			return "colNOTFOUND";
	}
	private static String getColNameinUI(String resourceName, String field) {
		try {
			String filter =  "tableName='"+resourceName+"' AND fieldName='"+field+"'";
//			String q = "random word £500 bank $";
			String filterEncode = URLEncoder.encode(filter, StandardCharsets.UTF_8.toString());
			boolean cache = UtilSessionData.getCache();
			JsonNode rowFT = JSonClient.get("main:FieldTemplate",filterEncode,cache,PropertyController.pre_conf_param_metadata,1+""); // normally cache is optional nut in this case , is very unlikely that this data NmaeinUI is changed
			for (JsonNode eachRow : rowFT)  {
				return eachRow.get("FieldNameInUI").asText();
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		String[] s = new String[]{"if (row.ALQUILADO==\"S\" || row.ALQUILADO==\"N\" || !row.ALQUILADO || row.ALQUILADO==\" \")\r\n" + 
//				"    return true;\r\n" + 
//				"else\r\n" + 
//				"    return false;","ALQ. sólo admite los valores S, N o blanco" };
		DataService.get().showError("el campo ("+field+") no exite en Metaconfig");
		return "colNOTFOUND";
	}


	public static String valueInList(String value, ValidationMetadata<?> metadata, String arg) {
		if (value == null || Arrays.asList(arg.split(",")).contains(value)) {
			return null;
		} else {
			return "El valor no se encuentra en la lista";
		}
	}

}
