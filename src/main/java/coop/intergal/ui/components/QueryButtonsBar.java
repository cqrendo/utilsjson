package coop.intergal.ui.components;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * Java wrapper of the polymer element `form-buttons-bar`
 */
@Tag("query-buttons-bar")
@JsModule("./src/components/query-buttons-bar.js")
public class QueryButtonsBar extends PolymerTemplate<TemplateModel> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id("bSearch")
	private Button bSearch;
	@Id("bCleanSearch")
	private Button bCleanSearch;
	@Id("bSQL")
	private Button bSQL;



	public static class SearchEvent extends ComponentEvent<QueryButtonsBar> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SearchEvent(QueryButtonsBar source, boolean fromClient) {
			super(source, fromClient);
		}
	}
	public static class ClearSearchEvent extends ComponentEvent<QueryButtonsBar> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ClearSearchEvent(QueryButtonsBar source, boolean fromClient) {
			super(source, fromClient);
		}
	}
	public static class SQLEvent extends ComponentEvent<QueryButtonsBar> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SQLEvent(QueryButtonsBar source, boolean fromClient) {
			super(source, fromClient);
		}
	}

	public Registration addSearchListener(ComponentEventListener<SearchEvent> listener) {
		bSearch.getElement().setAttribute("title", "ALT + B (buscar)") ;
//		bSearch.addClickShortcut(Key.KEY_B, KeyModifier.ALT); 
//		this.addAction(new ClickShortcut(searchButton, KeyCode.ENTER, null));
		bSearch.addClickShortcut(Key.ENTER); 
		return bSearch.addClickListener(e -> listener.onComponentEvent(new SearchEvent(this, true)));
}

	public Registration addClearSearchListener(ComponentEventListener<ClearSearchEvent> listener) {
		bCleanSearch.getElement().setAttribute("title", "ALT + C (limpiar búsqeda)") ;
		bCleanSearch.addClickShortcut(Key.KEY_C, KeyModifier.ALT).setBrowserDefaultAllowed(false);; 
		return bCleanSearch.addClickListener(e -> listener.onComponentEvent(new ClearSearchEvent(this, true)));
}
	public Registration addSQLListener(ComponentEventListener<SQLEvent> listener) {
		bSQL.getElement().setAttribute("title", "ALT + S (Búsqueda avanzada, crear SQL)") ;
		bSQL.addClickShortcut(Key.KEY_S, KeyModifier.ALT).setBrowserDefaultAllowed(false);; 
//		this.addAction(new ClickShortcut(searchButton, KeyCode.ENTER, null));
//		bSearch.addClickShortcut(Key.ENTER); 
		return bSQL.addClickListener(e -> listener.onComponentEvent(new SQLEvent(this, true)));
}
}