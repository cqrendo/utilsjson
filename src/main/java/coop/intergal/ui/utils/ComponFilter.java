package coop.intergal.ui.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;

import coop.intergal.AppConst;
import coop.intergal.vaadin.rest.utils.DataService;

public class ComponFilter {

	public static String prepareDateFilter(String field, String value) {
		if (value == null)
			return null;
		if (value.length() < 10)
		{
			DataService.get().showError("Fecha incorrecta demasiado corta , formatos admintidos( DD-MM-AAAA // DD-MM-AAAA hh:mm // DD-MM-AAAA::DD-MM-AAAA // DD-MM-AAAA hh:mm::DD-MM-AAAA hh:mm // >DD-MM-AAAA hh:mm..... )");
		// posible formats ( DD-MM-AAAA // DD-MM-AAAA hh:mm // DD-MM-AAAA::DD-MM-AAAA //
		// DD-MM-AAAA hh:mm::DD-MM-AAAA hh:mm // >DD-MM-AAAA hh:mm..... )
		// DD-MM-AAAA
			return null;
		}	
		if (value.indexOf("/") > 0)
			value = value.replaceAll("/", "-");
		if (value.startsWith("<") || value.startsWith(">")) {
			String op = "%3E";
			if (value.startsWith("<"))
				op = "%3C";
			String value1 = changeFromAAAAMMDDtoDDMMAAAA(value.substring(1));
			if (AppConst.FORMAT_FOR_SHORTDATETIME != null && AppConst.FORMAT_FOR_SHORTDATETIME.length() >1)
			{	
				value1 = AppConst.FORMAT_FOR_SHORTDATETIME.replaceAll("#value#",value1);
				return field + op +  value1.replaceAll(" ", "%20");
			}
			return field + op + "'" + value1.replaceAll(" ", "%20") + "'";
		}
		// DD-MM-AAAA HH:MM
		if (value.length() < 17) {
			String value1 = changeFromAAAAMMDDtoDDMMAAAA(value);
			String value2 = changeFromAAAAMMDDtoDDMMAAAANextDay(value);
			if (AppConst.FORMAT_FOR_SHORTDATETIME != null && AppConst.FORMAT_FOR_SHORTDATETIME.length() >1)
			{	
				value1 = AppConst.FORMAT_FOR_SHORTDATETIME.replaceAll("#value#",value1);
				value2 = AppConst.FORMAT_FOR_SHORTDATETIME.replaceAll("#value#",value2);
				return field + "%3E=" + value1.replaceAll(" ", "%20") + "%20AND%20" + field + "%3C"
				+ value2.replaceAll(" ", "%20");
			}
			return field + "%3E='" + value1.replaceAll(" ", "%20") + "'%20AND%20" + field + "%3C'"
					+ value2.replaceAll(" ", "%20") + "'";
		} else // a date range DD-MM-AAAA::DD-MM-AAAA or DD-MM-AAAA hh:mm::DD-MM-AAAA hh:mm
		{
			String[] tokens = value.split("::"); // the :: is use to avoid conflict with : of HH:mm
			String date1 = changeFromAAAAMMDDtoDDMMAAAA(tokens[0]);
			String date2 = changeFromAAAAMMDDtoDDMMAAAA(tokens[1]);
			if (date1.indexOf(":") == -1) // is time is not included then 23:59 is add to consider the last date
											// inclusive
				date1 = date1 + " 00:00:00";
			else
				date1 = date1 + ":00"; // the last time is complement with 59 seconds to be inclusive
			if (date2.indexOf(":") == -1) // is time is not included then 23:59 is add to consider the last date
				// inclusive	
				date2 = date2 + " 23:59:59";
			else
				date2 = date2 + ":59"; // the last time is complement with 59 seconds to be inclusive

			if (AppConst.FORMAT_FOR_DATETIME != null && AppConst.FORMAT_FOR_SHORTDATETIME.length() >1)
			{	
			//	date2.replaceAll(" 23:59:59", "T23:59:59" );  
				date1 = AppConst.FORMAT_FOR_DATETIME.replaceAll("#value#",date1);
				date2 = AppConst.FORMAT_FOR_DATETIME.replaceAll("#value#",date2);
				return field + "%3E=" + date1.replaceAll(" ", "%20") + "%20AND%20" + field + "%3C="
				+ date2.replaceAll(" ", "%20");
			}
			return field + "%3E='" + date1.replaceAll(" ", "%20") + "'%20AND%20" + field + "%3C='"
					+ date2.replaceAll(" ", "%20") + "'";

		}
	}

	private static String changeFromAAAAMMDDtoDDMMAAAA(String value) {
		String vDay, vMonth, vYear;
		StringTokenizer tokens = new StringTokenizer(value, "-");
		vDay = tokens.nextToken();
		vMonth = tokens.nextToken();
		vYear = tokens.nextToken();
		if (vYear.length() > 4) // then it comes in the format DD-MM-AAAA HH:MM
		{
			vDay = vDay + vYear.substring(4);
			vYear = vYear.substring(0, 4);

		}
		return vYear + "-" + vMonth + "-" + vDay;
	}

	private static String changeFromAAAAMMDDtoDDMMAAAANextDay(String value) {
		value = changeFromAAAAMMDDtoDDMMAAAA(value);
//		String dt = "2008-01-01";  // Start date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		int timeToAdd;
		if (value.length() < 11) // only date not time
		{
			timeToAdd = Calendar.DATE;
		} else {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			timeToAdd = Calendar.MINUTE;
		}

		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(value));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c.add(timeToAdd, 1); // number of days to add
		value = sdf.format(c.getTime()); // dt is now the new date
		return value;
	}
	public static String componeNumberFilter(String value) {
		if (value.indexOf(":")> 0)  // is a range
			return componeNumberRange(value);
		else if (value.indexOf(",")> 0)  // is a list
			return componeNumberList(value);
		else
			return determineOperator(value);
	}
	public static String componeNumberRange(String value) { // determines to use = like or > <
		value = value.trim(); // deletes extra blanks pre and post
		int posColon = value.indexOf(":");
		String fromNumber = value.substring(0,posColon );
		String toNumber = value.substring(posColon+1 );
		
		return "%20BETWEEN%20" + fromNumber + "%20AND%20" + toNumber ;
	}
	public static String componeNumberList(String value) {
		value = value.trim(); // deletes extra blanks pre and post
		return "%20IN("+value+")";
	}
	public static String componeTextList(String value) {
		value = value.trim(); // deletes extra blanks pre and post
		value= value.replace(",","','");
		return "%20IN('"+value+"')";
	}
	public static String determineOperator(String value) { // determines to use = like or > <
		value = value.trim(); // deletes extra blanks pre and post
		if (value.indexOf("IN(") > -1 ||value.indexOf("='") > -1  || value.indexOf("%20like('") >-1 || value.indexOf("%20BETWEEN%20") > -1) // the operator is already determined
			return value;
		if (value.indexOf("%") >= 0) {
			value = value.replaceAll("%", "%25");
			if (value.indexOf(" ") >= 0)
				value = value.replaceAll(" ", "%20");
			return "%20like('" + value + "')";
		}
		if (value.indexOf("<") == 0) {
			return value.replaceAll("<", "%3C");
		}
		if (value.indexOf(">") == 0) {
			return value.replaceAll(">", "%3E");
		}
		if (value.startsWith("=")) // in the expert SQL = it comes with the value "=value" , in normal query only the value
			return "='" + value.substring(1) + "'";
		return "='" + value + "'";
	}

}
