package coop.intergal.ui.views;

import static coop.intergal.AppConst.PAGE_PRODUCTS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.polymertemplate.TemplateParser;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.templatemodel.TemplateModel;

import coop.intergal.PropertyController;
import coop.intergal.espresso.presutec.utils.JSonClient;
import coop.intergal.ui.util.UtilSessionData;
import coop.intergal.ui.utils.ComponFilter;
import coop.intergal.vaadin.rest.utils.DynamicDBean;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.H5;





//@Tag("dynamic-view-grid")
@Tag("expert-sql-form")
@JsModule("./src/views/generic/forms/expert-sql-form.js")
//@Route(value = PAGE_DYNAMIC+"forPICK")//, layout = MainView.class)
//@PageTitle(AppConst.TITLE_PRODUCTS)
//@Secured(Role.ADMIN)
//@Uses(ArticulosQueryForPick.class)  TODO check is works with last releases of vaadin
//@Uses(ProveedorQueryForPick.class) 
public class ExpertSqlForm extends PolymerTemplate<TemplateModel> implements  HasDynamicTitle  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private final Binder<DynamicDBean> binder = new Binder<>(DynamicDBean.class);
	

//	private CurrencyFormatter currencyFormatter = new CurrencyFormatter();

//	private String resourceName;
	private String title;
//	private String filter;
	private static String mapedCols;
	@Id("acceptPick")
	private Button acceptPick;
	@Id("vlComboTwin1")
	private HorizontalLayout vlComboTwin1;
	@Id("vlComboTwin4")
	private HorizontalLayout vlComboTwin4;
	@Id("vlComboTwin5")
	private HorizontalLayout vlComboTwin5;
	@Id("vlComboTwin6")
	private HorizontalLayout vlComboTwin6;
	@Id("vlComboTwin7")
	private HorizontalLayout vlComboTwin7;
	@Id("vlComboTwin8")
	private HorizontalLayout vlComboTwin8;
	@Id("vlComboTwin9")
	private HorizontalLayout vlComboTwin9;
	@Id("vaadinHorizontalLayout")
	private HorizontalLayout vaadinHorizontalLayout;
//	@Id("dummy")
//	private Button removeCombi1;
	@Id("cBField1")
	private ComboBox<DynamicDBean> cBField1;
	@Id("cBOperator1")
	private ComboBox<String>  cBOperator1;
	@Id("tf1")
	private TextField tf1;
	@Id("cBField2")
	private ComboBox<DynamicDBean> cBField2;
	@Id("cBOperator2")
	private ComboBox<String> cBOperator2;
	@Id("tf2")
	private TextField tf2;
	@Id("vlComboTwin3")
	private HorizontalLayout vlComboTwin3;
	@Id("removeCombi3")
	private Button removeCombi3;
	@Id("cBField3")
	private ComboBox<DynamicDBean> cBField3;
	@Id("cBOperator3")
	private ComboBox<String> cBOperator3;
	@Id("tf3")
	private TextField tf3;

	@Id("removeCombi4")
	private Button removeCombi4;
	@Id("cBField4")
	private ComboBox<DynamicDBean> cBField4;
	@Id("cBOperator4")
	private ComboBox<String> cBOperator4;
	@Id("tf4")
	private TextField tf4;

	@Id("vlComboTwin2")
	private HorizontalLayout vlComboTwin2;
	@Id("removeCombi2")
	private Button removeCombi2;
	@Id("removeCombi5")
	private Button removeCombi5;
	@Id("cBField5")
	private ComboBox<DynamicDBean> cBField5;
	@Id("cBOperator5")
	private ComboBox<String> cBOperator5;
	@Id("tf5")
	private TextField tf5;

	@Id("removeCombi6")
	private Button removeCombi6;
	@Id("cBField6")
	private ComboBox<DynamicDBean> cBField6;
	@Id("cBOperator6")
	private ComboBox<String> cBOperator6;
	@Id("tf6")
	private TextField tf6;

	@Id("removeCombi7")
	private Button removeCombi7;
	@Id("cBField7")
	private ComboBox<DynamicDBean> cBField7;
	@Id("cBOperator7")
	private ComboBox<String> cBOperator7;
	@Id("tf7")
	private TextField tf7;

	@Id("removeCombi8")
	private Button removeCombi8;
	@Id("cBField8")
	private ComboBox<DynamicDBean> cBField8;
	@Id("cBOperator8")
	private ComboBox<String> cBOperator8;
	@Id("tf8")
	private TextField tf8;

	@Id("removeCombi9")
	private Button removeCombi9;
	@Id("cBField9")
	private ComboBox<DynamicDBean> cBField9;
	@Id("cBOperator9")
	private ComboBox<String> cBOperator9;
	@Id("tf9")
	private TextField tf9;

	@Id("addNewCombo")
	private Button addNewCombo;

	Collection<DynamicDBean> dbeanListChild  ;
	Collection<DynamicDBean> dbeanListParent ;
//	@Id("dummy")
//	private Button dummy;
	@Id("textSql")
	private TextArea textSql;
	@Id("cBAndOr1")
	private ComboBox<String> cBAndOr1;
	@Id("cBAndOr2")
	private ComboBox<String> cBAndOr2;
	@Id("cBAndOr3")
	private ComboBox<String> cBAndOr3;
	@Id("cBAndOr4")
	private ComboBox<String> cBAndOr4;
	@Id("cBAndOr5")
	private ComboBox<String> cBAndOr5;
	@Id("cBAndOr6")
	private ComboBox<String> cBAndOr6;
	@Id("cBAndOr7")
	private ComboBox<String> cBAndOr7;
	@Id("cBAndOr8")
	private ComboBox<String> cBAndOr8;
	@Id("cBAndOr9")
	private ComboBox<String> cBAndOr9;
	

	@Id("isAutomatic")
	private Checkbox isAutomatic;

	
	public String getTextSql() {
		return textSql.getValue();
	}
	private ArrayList <String> rowsColList; //= getRowsCnew String[] { "code_customer", "name_customer", "cif", "amountUnDisbursedPayments" };
//	private String preConfParam;
	private String textSQL;


	private String lastAndOr = "AND";


	private int lastLevel;


	@Id("h5AyudaSQL")
	private H5 h5AyudaSQL;


	public String getLastAndOr() {
		return lastAndOr;
	}

	public void setLastAndOr(String lastAndOr) {
		this.lastAndOr = lastAndOr;
	}

	public ArrayList<String> getRowsColList() {
		return rowsColList;
	}

	public void setRowsColList(ArrayList<String> rowsColList) {
		this.rowsColList = rowsColList;
	}	

	public ExpertSqlForm(String table) {
		super();
		textSql.setWidthFull();
		isAutomatic.setValue(true);
		isAutomatic.addValueChangeListener(e ->refreshSQL(e.getValue()));
		isAutomatic.setLabel("SQL Automático");
		textSql.setEnabled(false);
		tf1.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf1.addValueChangeListener(e -> fillTextArea(1));
		tf2.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf2.addValueChangeListener(e -> fillTextArea(2));
		tf3.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf3.addValueChangeListener(e -> fillTextArea(3));
		tf4.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf4.addValueChangeListener(e -> fillTextArea(4));
		tf5.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf5.addValueChangeListener(e -> fillTextArea(5));
		tf6.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf6.addValueChangeListener(e -> fillTextArea(6));
		tf7.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf7.addValueChangeListener(e -> fillTextArea(7));
		tf8.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf8.addValueChangeListener(e -> fillTextArea(8));
		tf9.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		tf9.addValueChangeListener(e -> fillTextArea(9));
		
		cBAndOr1.addValueChangeListener(e -> keepLastAndOr(e.getValue(), 1));
		cBAndOr2.addValueChangeListener(e -> keepLastAndOr(e.getValue(),2));
		cBAndOr3.addValueChangeListener(e -> keepLastAndOr(e.getValue(),3));
		cBAndOr4.addValueChangeListener(e -> keepLastAndOr(e.getValue(),4));
		cBAndOr5.addValueChangeListener(e -> keepLastAndOr(e.getValue(),5));
		cBAndOr6.addValueChangeListener(e -> keepLastAndOr(e.getValue(),6));
		cBAndOr7.addValueChangeListener(e -> keepLastAndOr(e.getValue(),7));
		cBAndOr8.addValueChangeListener(e -> keepLastAndOr(e.getValue(),8));
		cBAndOr9.addValueChangeListener(e -> keepLastAndOr(e.getValue(),9));
	

		

//		removeCombi1.setDisableOnClick(true);
		addNewCombo.addClickListener(e -> showNext());
		removeCombi2.addClickListener(e -> vlComboTwin2.setVisible(false));
		removeCombi3.addClickListener(e -> vlComboTwin3.setVisible(false));
		removeCombi4.addClickListener(e -> vlComboTwin4.setVisible(false));
		removeCombi5.addClickListener(e -> vlComboTwin5.setVisible(false));
		removeCombi6.addClickListener(e -> vlComboTwin6.setVisible(false));
		removeCombi7.addClickListener(e -> vlComboTwin7.setVisible(false));
		removeCombi8.addClickListener(e -> vlComboTwin8.setVisible(false));
		removeCombi9.addClickListener(e -> vlComboTwin9.setVisible(false));
		acceptPick.addClickShortcut(Key.F10);
		hideAll();
//		cBField1.setLabel(childResourceName);
//		cBOperator1.setLabel(parentResourceName);
		fillColletions(table);
		fillCombo(1);

//		setupGrid();
		
	}

	private Object refreshSQL(Boolean isMarkedAutomatic) {
		if (isMarkedAutomatic)
			{
			fillTextArea(lastLevel);
			textSql.setEnabled(false);
			isAutomatic.setLabel("SQL Automático");
			h5AyudaSQL.setText("SQL Automático. Se genera según los valores superiores");
			}
		else
		{
			textSql.setEnabled(true);
			isAutomatic.setLabel("SQL Automático. Si lo marcamos, se regenera el código");
			h5AyudaSQL.setText("SQL Manual. No se ve afectado por los valores superiores");

		}
		return null;
	}

	private Object keepLastAndOr(String value, int i) {
		if (lastLevel == i )
			lastAndOr = value;
		return null;
	}

	private Object fillTextArea(int i) {
		if (lastLevel < i)
			lastLevel = i;
		if (isAutomatic.getValue())
		{
		if (i > 0)
			{
		   	 textSQL = "";
		   	 textSQL = textSQL + mountFieldOperatorAndValue(cBField1.getValue().getCol0(), cBOperator1.getValue(), tf1.getValue(), cBField1.getValue().getCol1()) + "\n";
		   	 textSql.setValue(textSQL);
		   	 lastAndOr = cBAndOr1.getValue();
			}
		if (i > 1)	
			{
		   	 textSQL = textSQL + " " +cBAndOr1.getValue()+ " "  + mountFieldOperatorAndValue(cBField2.getValue().getCol0(), cBOperator2.getValue(), tf2.getValue(), cBField2.getValue().getCol1())+ "\n";
		   	 textSql.setValue(textSQL);
		   	 lastAndOr = cBAndOr2.getValue();
			}
		if (i > 2)	
			{
		   	 textSQL = textSQL + " " + cBAndOr2.getValue() + " " + mountFieldOperatorAndValue(cBField3.getValue().getCol0(), cBOperator3.getValue(), tf3.getValue(), cBField3.getValue().getCol1())+ "\n";
		   	 textSql.setValue(textSQL);
		   	 lastAndOr = cBAndOr3.getValue();
			}
		}
		return null;
		
	}

	private String mountFieldOperatorAndValue(String field, String operator, String value, String type) {
		
		String newFilter = "";
		if (type.equals("timestamp"))
		{
			String valueWithOperator = value;
			if (operator.equals(">") || operator.equals("<"))
				valueWithOperator  = operator+value;
			String dateFilter = ComponFilter.prepareDateFilter(field, valueWithOperator);
				newFilter = dateFilter;
		}
		else if (type.equals("number"))
			newFilter =	field + ComponFilter.componeNumberFilter(operator+value);
		else if (value.indexOf(",") >-1)
			newFilter =	 field + ComponFilter.componeTextList(value);
		else if (operator.equals("like"))
		{
			if (value.indexOf("%") > -1)
				newFilter =	field+" like('" + value + "')";
			else
				newFilter =	 field+" like('%" + value + "%')";
		}
		else
		{
			newFilter =	 field+operator+"'"+value+"'";
		}
		newFilter = newFilter.replaceAll("%20"," ");
		newFilter = newFilter.replaceAll("%3E",">");
		newFilter = newFilter.replaceAll("%3C","<");
		newFilter = newFilter.replaceAll("%28","(");
		newFilter = newFilter.replaceAll("%29",")");

		return newFilter;
	}

	public ExpertSqlForm(TemplateParser parser, VaadinService service) {
		super(parser, service);
		// TODO Auto-generated constructor stub
	}

	public ExpertSqlForm(TemplateParser parser) {
		super(parser);
		// TODO Auto-generated constructor stub
	}



	protected String getBasePage() {
		return PAGE_PRODUCTS;
	}

//	@Override
	protected Binder<DynamicDBean> getBinder() {
		return binder;
	}

	public Button showButtonClickedMessage()
	{
		return null;
		
	}


	private void fillColletions(String table) {
		
		if (table.startsWith("CR-"))
			table = table.substring(3);
		int idxSubFix = table.indexOf("__");
		if (idxSubFix >-1)
			table = table.substring(0,idxSubFix);
		
		dbeanListChild = new ArrayList<DynamicDBean>();
		ArrayList<String[]> rowsColList = new ArrayList<String[]>();
		String[] fieldArr  = new String[3];
		fieldArr[0]  = "fieldName";
		fieldArr[1] = "";
		fieldArr[2] = "col0";	
		rowsColList.add(fieldArr);
		fieldArr  = new String[3];
		fieldArr[0]  = "dataType";
		fieldArr[1] = "";
		fieldArr[2] = "col1";
		rowsColList.add(fieldArr);
		JsonNode colFromTable = null;
		String allowPrefixTable = "CompanyYear,GFER,TYSH,FISCALEURO,GHOS";  // @@TODO params CompanyYear means the chosen company to work
	
		StringTokenizer tokens = new StringTokenizer(allowPrefixTable,",");
	//	boolean anyError = false; 
		while (tokens.hasMoreElements())
		{
			String eachPrefix = tokens.nextToken();
			if (eachPrefix.equals("CompanyYear"))
				eachPrefix = UtilSessionData.getCompanyYear();
			colFromTable = JSonClient.getColumnsFromTable(eachPrefix+ ":"+ table, null, false, UtilSessionData.getCompanyYear()+PropertyController.pre_conf_param);
			if (colFromTable.get(0).fieldNames() != null && colFromTable.get(0).fieldNames().hasNext())
				break;
		}
		Iterator<String> fN = colFromTable.get(0).fieldNames();
		while (fN.hasNext()) 
		{
			DynamicDBean db = new DynamicDBean();
			String fieldName = fN.next();
			String type = colFromTable.get(0).get(fieldName).asText();	
			db.setCol0(fieldName);			
			db.setCol1(type);
			dbeanListChild.add(db);
		}
	}  

	private void fillCombo(int line) {
		switch (line)
		{
		     case 1:
		 		cBField1.setItems(dbeanListChild);
				cBField1.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator1.setItems(getOperators());
				cBOperator1.setValue("=");
				cBAndOr1.setItems("AND","OR");cBAndOr1.setValue("AND");
				break;
//				cBOperator1.setItemLabelGenerator(DynamicDBean::getCol0);
		     
		     case 2:
			 	cBField2.setItems(dbeanListChild);
				cBField2.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator2.setItems(getOperators());
				cBOperator2.setValue("=");
				cBAndOr2.setItems("AND","OR");cBAndOr2.setValue("AND");
//				cBOperator2.setItems(getOperators());
//				cBOperator2.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 3:
			 	cBField3.setItems(dbeanListChild);
				cBField3.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator3.setItems(getOperators());
				cBOperator3.setValue("=");
				cBAndOr3.setItems("AND","OR");cBAndOr3.setValue("AND");
//				cBOperator3.setItems(dbeanListParent);
//				cBOperator3.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 4:
			 	cBField4.setItems(dbeanListChild);
				cBField4.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator4.setItems(getOperators());
				cBOperator4.setValue("=");
				cBAndOr4.setItems("AND","OR");cBAndOr4.setValue("AND");
//				cBOperator4.setItems(dbeanListParent);
//				cBOperator4.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 5:
			 	cBField5.setItems(dbeanListChild);
				cBField5.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator5.setItems(getOperators());
				cBOperator5.setValue("=");
				cBAndOr5.setItems("AND","OR");cBAndOr5.setValue("AND");
//				cBOperator5.setItems(dbeanListParent);
//				cBOperator5.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 6:
			 	cBField6.setItems(dbeanListChild);
				cBField6.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator6.setItems(getOperators());
				cBOperator6.setValue("=");
				cBAndOr6.setItems("AND","OR");cBAndOr6.setValue("AND");
//				cBOperator6.setItems(dbeanListParent);
//				cBOperator6.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 7:
			 	cBField7.setItems(dbeanListChild);
				cBField7.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator7.setItems(getOperators());
				cBOperator7.setValue("=");
				cBAndOr7.setItems("AND","OR");cBAndOr7.setValue("AND");
//				cBOperator7.setItems(dbeanListParent);
//				cBOperator7.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 8:
			 	cBField8.setItems(dbeanListChild);
				cBField8.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator8.setItems(getOperators());
				cBOperator8.setValue("=");
				cBAndOr8.setItems("AND","OR");cBAndOr8.setValue("AND");
//				cBOperator8.setItems(dbeanListParent);
//				cBOperator8.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     case 9:
			 	cBField9.setItems(dbeanListChild);
				cBField9.setItemLabelGenerator(DynamicDBean::getCol0);
				cBOperator9.setItems(getOperators());
				cBOperator9.setValue("=");
				cBAndOr9.setItems("AND","OR");cBAndOr9.setValue("AND");
//				cBOperator9.setItems(dbeanListParent);
//				cBOperator9.setItemLabelGenerator(DynamicDBean::getCol0);
				break;
		     default:
//			 	cBField1.setItems(dbeanListChild);
//				cBField1.setItemLabelGenerator(DynamicDBean::getCol0);
//				cBOperator1.setItems(dbeanListParent);
//				cBOperator1.setItemLabelGenerator(DynamicDBean::getCol0);		     ;
		}

		
	}

	private String[] getOperators() {
		// TODO Auto-generated method stub
		return  new String[] {"=",">","<","like","Range","List"};
	}

	private void hideAll() {
		vlComboTwin2.setVisible(false);
		vlComboTwin3.setVisible(false);
		vlComboTwin4.setVisible(false);
		vlComboTwin5.setVisible(false);
		vlComboTwin6.setVisible(false);
		vlComboTwin7.setVisible(false);
		vlComboTwin8.setVisible(false);
		vlComboTwin9.setVisible(false);
		
	}
	private void showNext() {
		
		if (vlComboTwin8.isVisible())
		{
			fillCombo(9);
			vlComboTwin9.setVisible(true);
		}
		else if (vlComboTwin7.isVisible())
		{
			fillCombo(8);
			vlComboTwin8.setVisible(true);
		}
		else if (vlComboTwin6.isVisible())
		{
			fillCombo(7);
			vlComboTwin7.setVisible(true);
		}
		else if (vlComboTwin5.isVisible())
		{
			fillCombo(6);
			vlComboTwin6.setVisible(true);
		}
		else if (vlComboTwin4.isVisible())
		{
			fillCombo(5);
			vlComboTwin5.setVisible(true);
		}
		else if (vlComboTwin3.isVisible())
		{
			fillCombo(4);
			vlComboTwin4.setVisible(true);
		}
		else if (vlComboTwin2.isVisible())
		{
			fillCombo(3);
			vlComboTwin3.setVisible(true);
		}
		else if (vlComboTwin1.isVisible())
		{
			fillCombo(2);
			vlComboTwin2.setVisible(true);
		}

		
	}
	@Override
	public String getPageTitle() {
		return title;
	}

	public static class AcceptPickEvent extends ComponentEvent<ExpertSqlForm> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public AcceptPickEvent(ExpertSqlForm source, boolean fromClient) {
			
			super(source, fromClient);
		}
	}

	public Registration addAcceptPickListener(ComponentEventListener<AcceptPickEvent> listener) {
	return acceptPick.addClickListener(e -> {listener.onComponentEvent(new AcceptPickEvent(this, true));});
}




	public String getMapedCols() {
		// TODO Auto-generated method stub
		return mapedCols;
	}


//	@Override
//	protected CrudEntityPresenter<DynamicDBean> getPresenter() {
//		// TODO Auto-generated method stub
//		return null;
//	}
}
