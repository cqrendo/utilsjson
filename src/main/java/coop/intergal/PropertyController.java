package coop.intergal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

//@SpringComponent
//@RestController
@ConfigurationProperties
public class PropertyController {
 public static String pre_conf_param_metadata;
 public static String pre_conf_param;
 public static String package_views;
 public static String count_used_by_db;
 public static String login_name;
 public static String login_forgetpass_content;
 public static String login_forgetpass_error;
 public static String login_forgetpass_notemail;
 public static String login_description;
 public static String email_remitente;
 public static String email_clave;
 public static String email_host;
 public static String email_port;
 public static String ldap_base;
 public static String url_fotos;
 public static String url_docs;
 public static String confirm_delete;
 public static String value_true_for_booleans;
 public static String value_false_for_booleans;
 public static String clear_report_server;
 public static String include_main_layout;
 
 public PropertyController(
 @Value("${app.const.pre_conf_param_metadata}") 
 String property1, 
 @Value("${app.const.pre_conf_param}") 
 String property2,
 @Value("${app.const.package_views}")
 String property3,
 @Value("${app.const.count_used_by_db}")
 String property4,
 @Value("${app.const.login_name}")
 String property5,
 @Value("${app.const.login_forgetpass_content}")
 String property6,
 @Value("${app.const.login_forgetpass_error}")
 String property7,
 @Value("${app.const.login_forgetpass_notemail}")
 String property8,
 @Value("${app.const.email_remitente}")
 String property9,
 @Value("${app.const.email_clave}")
 String property10,
 @Value("${app.const.email_host}")
 String property11,
 @Value("${app.const.email_port}")
 String property12,
 @Value("${app.const.ldap_base}")
 String property13,
 @Value("${app.const.url_fotos}")
 String property14,
 @Value("${app.const.url_docs}")
 String property15,
 @Value("${app.const.confirm_delete}")
 String property16,
 @Value("${app.const.value_true_for_booleans}")
 String property17,
 @Value("${app.const.value_false_for_booleans}")
 String property18,
 @Value("${app.const.clear_report_server}")
 String property19,
 @Value("${app.const.login_description}")
 String property20,
 @Value("${app.const.include_main_layout}")
 String property21
 ) 
 {
	 System.out.println("PropertyController.PropertyController() " 
 + property1 + property2 + property3 + property4 + property5 + property6 + property7 + property8 + property9 + property10 
 + property11 + property12 + property13 + property14 + property15 + property16 + property17 + property18 + property19 );
 PropertyController.pre_conf_param_metadata = property1;
 PropertyController.pre_conf_param = property2;
 PropertyController.package_views = property3;
 PropertyController.count_used_by_db = property4;
 PropertyController.login_name = property5;
 PropertyController.login_forgetpass_content = property6; 
 PropertyController.login_forgetpass_error = property7; 
 PropertyController.login_forgetpass_notemail = property8; 
 PropertyController.email_remitente = property9; 
 PropertyController.email_clave = property10; 
 PropertyController.email_host = property11; 
 PropertyController.email_port = property12; 
 PropertyController.ldap_base = property13; 
 PropertyController.url_fotos = property14; 
 PropertyController.url_docs = property15; 
 PropertyController.confirm_delete = property16; 
 PropertyController.value_true_for_booleans = property17; 
 PropertyController.value_false_for_booleans = property18; 
 PropertyController.clear_report_server = property19; 
 PropertyController.login_description = property20; 
 PropertyController.include_main_layout = property21; 
 }
}
